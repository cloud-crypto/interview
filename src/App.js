import './App.css';

import React, { useState, useEffect } from 'react';
import { Row, Form} from 'react-bootstrap';
import { CeramicClient } from '@ceramicnetwork/http-client'
import { TileDocument } from '@ceramicnetwork/stream-tile'
// import 'bootstrap/dist/css/bootstrap.min.css';

const API_URL = 'https://ceramic-node.akiprotocol.io';
const INDEX_STREAM_ID = 'k2t6wyfsu4pfybfs4jxrebwdl4u2fidc9biqjpofot4x9qjhdytejote9evjta';
const ceramic = new CeramicClient(API_URL);

function App() {
  const [index, setIndex] = useState({});
  const [walletInteractions, setWalletInteractions] = useState({});

  useEffect(() => {
    const fetchIndex = async () => {
      const indexDoc = await TileDocument.load(ceramic, INDEX_STREAM_ID);
      setIndex(indexDoc.content);
    };

    fetchIndex();
  }, []);

  const fetchWalletInteractions = async (address) => {
    const upperAddress = address.toUpperCase();
    const entry = index[upperAddress];
    const streamId = entry?.[56]; // Only support 56 BSC for now
    if (streamId) {
      const walletDoc = await TileDocument.load(ceramic, streamId);
      if (walletDoc.content) {
        setWalletInteractions(walletDoc.content);
      }
    }
  };

  return (
    <>
      <div className='m-5'>
        <h1>Ceramic Explorer</h1>
        <Row>
          <Form.Control 
            type="walletAddress"
            placeholder="Enter wallet address" 
            onChange={async (e) => {
              await fetchWalletInteractions(e.target.value);
            }}/>
        </Row>
        <Row>
          <h2>Wallet History</h2>
          <div><pre>
            {walletInteractions.interactions && JSON.stringify(walletInteractions.interactions.map(
              v => {
                return {
                  address: v.wallet.address,
                  reason: v.attribution.reason,
                  metadata: v.attribution.metadata,
                  time: v.attribution.timestamp,
                };
              }
            ), null, 2)}
          </pre></div>
        </Row>
      </div>
    </>
  );
}

export default App;
